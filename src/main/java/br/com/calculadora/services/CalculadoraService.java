package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){

        Integer resultado = 0;
        for (Integer numero : calculadora.getNumeros()){
            resultado += numero;
        }

        return new RespostaDTO(resultado);
    }

    public RespostaDTO subtrair(Calculadora calculadora){

        Integer resultado = 0;
        for (Integer numero : calculadora.getNumeros()) {
            resultado -= numero;
        }

        return new RespostaDTO(resultado);
    }

    public boolean isNatural(Calculadora calculadora) {
        for (Integer numero : calculadora.getNumeros()) {
            if(numero < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isDenominadorMaior(Calculadora calculadora){
        if(calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1)) {
            return false;
        }
        return true;
    }

    public boolean isDivisaoValida(Calculadora calculadora){
        if(calculadora.getNumeros().size() > 2){
            return false;
        }
        return true;
    }

    public RespostaDTO dividir(Calculadora calculadora) {
        Integer resultado;
        resultado = calculadora.getNumeros().get(0)/calculadora.getNumeros().get(1);
        return new RespostaDTO(resultado);
    }

    public RespostaDTO multiplicar(Calculadora calculadora) {
        Integer resultado=1;
        for (Integer numero : calculadora.getNumeros()) {
            resultado *= numero;
        }
        return new RespostaDTO(resultado);
    }
}
