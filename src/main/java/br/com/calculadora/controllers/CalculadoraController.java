package br.com.calculadora.controllers;


import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário informar 2 número para realizar essa operação.");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário informar 2 número para realizar essa operação.");
        }
        else if(!calculadoraService.isNatural(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "São aceitos apenas números naturais para essa operação.");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário informar 2 número para realizar essa operação.");
        }
        else if(!calculadoraService.isNatural(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "São aceitos apenas números naturais para essa operação.");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário informar 2 números apenas para realizar essa operação.");
        }
        else if(!calculadoraService.isDivisaoValida(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário informar 2 números apenas para realizar essa operação.");
        }
        else if(!calculadoraService.isDenominadorMaior(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é possível dividir um número por outro maior que ele.");
        }
        else if(!calculadoraService.isNatural(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "São aceitos apenas números naturais para essa operação.");
        }

        return calculadoraService.dividir(calculadora);
    }

}
